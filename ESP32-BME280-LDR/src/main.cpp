#include "secrets.h"
#include <MQTTClient.h>   
#include <WiFiClientSecure.h>
#include <ArduinoJson.h>

#include <Wire.h>               
#include <Adafruit_BME280.h>

#ifdef __cplusplus
extern "C" {
#endif

uint8_t temprature_sens_read();

#ifdef __cplusplus
}
#endif

#define WIFI_TIMEOUT_MS 20000         // 20 second WiFi connection timeout
#define WIFI_RECOVER_TIME_MS 30000    // Wait 30 seconds after a failed connection attempt
#define AWS_IOT_PUBLISH_TOPIC         "WEATHER/ESP32/BME280/UTRE32BME280LDR001"
#define AWS_IOT_SUBSCRIBE_TOPIC       "CMD/ESP32/BME280/UTRE32BME280LDR001"
#define SEALEVELPRESSURE_HPA (1023.00)

Adafruit_BME280 bme; // I2C

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;
 
#define LIGHT_SENSOR_PIN 34   // ESP32 pin (ADC0)
#define DHTPIN 27             // Digital pin connected to the DHT sensor
#define DHTTYPE DHT22         // DHT 22  (AM2302), AM2321

WiFiClientSecure net = WiFiClientSecure();
MQTTClient client = MQTTClient(256);  

void messageHandler(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);

  //  StaticJsonDocument<200> doc;
  //  deserializeJson(doc, payload);
  //  const char* message = doc["message"];
}

// Function that gets current epoch time
unsigned long getTime() {
  time_t now;
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return(0);
  }
  time(&now);
  return now;
}

bool checkNTP_TimeSync()
{
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return false;
  }
  return true;
}

void printLocalTime()
{
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}

int median_of_3(int a, int b, int c){
    int the_max = max(max(a, b), c);
    int the_min = min(min(a, b), c);
    int the_median = the_max ^ the_min ^ a ^ b ^ c;
    //bitwise xor operator used here https://www.arduino.cc/reference/en/language/structure/bitwise-operators/bitwisexor/  
    return( the_median );
}

int read_Light()
{
  int result;
  result = analogRead(LIGHT_SENSOR_PIN);                   //always throw away the first reading!
  
  delay(15);
  result = median_of_3(analogRead(LIGHT_SENSOR_PIN), analogRead(LIGHT_SENSOR_PIN), analogRead(LIGHT_SENSOR_PIN));
  
  return result;
}

StaticJsonDocument<200> read_Sensor_Data()
{
  StaticJsonDocument<200> doc;

  doc["time"]         = getTime();                                  //Epoc time since 1970 with NTP correction
  doc["espTemp"]      = (temprature_sens_read() - 32) / 1.8;        //ESP32 onboard temp
  doc["hallData"]     = hallRead();                                 //ESP32 onboard hall/magentic sensor
  doc["tempCelsius"]  = bme.readTemperature();                      //BME280 Temp in C
  doc["humidity"]     = bme.readHumidity();                         //BME280 Hum
  doc["pressure"]     = bme.readPressure() / 100.0F;                //BME280 Pressure
  doc["altitude"]     = bme.readAltitude(SEALEVELPRESSURE_HPA);     //BME280 Altitude with sea-level correction
  doc["light"]        = read_Light();                               //LDR light sensor with 10K resitor

  return doc;
}

void publish_BME280_Data()
{
  char jsonBuffer[512];
  serializeJson(read_Sensor_Data(), jsonBuffer);

  client.publish(AWS_IOT_PUBLISH_TOPIC, jsonBuffer);
}

//CheckBME280Data
void CheckSensorData( void * pvParameters ) {
  for(;;) {
    Serial.print(">>> CheckSensorData running on ESP32 core in background");
    Serial.println(xPortGetCoreID());
    
    if(client.connected())
    {
      publish_BME280_Data();

      //sleep for 1min(60000) or can change to 10min (600000)
      vTaskDelay(60000 / portTICK_PERIOD_MS);
      continue;
    }
    else 
    {
      //sleep for 10sec
      vTaskDelay(10000 / portTICK_PERIOD_MS);
      continue;
    }
  }
}

void keepWiFiAlive(void * parameter) {
  unsigned long startAttemptTime = 0;
  bool subscribeToTopic = false;
  bool ntpTimeSyncDone = false;
  bool wifiSetupDone = false;
  bool awsIoTSetupDone = false;

  for(;;) {
    if(WiFi.status() == WL_CONNECTED && client.loop() && subscribeToTopic) {
        vTaskDelay(10000 / portTICK_PERIOD_MS);
        ntpTimeSyncDone = false;
        wifiSetupDone = false;
        awsIoTSetupDone = false;
        
        continue;
    }
    
    Serial.print(">>> keepWiFiAlive running on core ");
    Serial.println(xPortGetCoreID());

    if(WiFi.status() != WL_CONNECTED) {
      Serial.println("[WIFI] Connecting");

      WiFi.mode(WIFI_STA);
      WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

      // Keep looping while we're not connected and haven't reached the timeout
      startAttemptTime = millis();
      while (WiFi.status() != WL_CONNECTED &&  millis() - startAttemptTime < WIFI_TIMEOUT_MS) {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
      }
    }

    if(WiFi.status() != WL_CONNECTED) {
      Serial.println("[WIFI] FAILED, retrying in 30 sec...");
      vTaskDelay(WIFI_RECOVER_TIME_MS / portTICK_PERIOD_MS);
      continue;
    }
    else {  // We connected at this stage to wifi
      if(!wifiSetupDone) {
        Serial.print("[WIFI] Conntected with IP: ");
        Serial.println(WiFi.localIP());
        wifiSetupDone = true;
      }

      if(!ntpTimeSyncDone) {
        Serial.println("Obtaining NTP time-sync...");
        configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
        
        startAttemptTime = millis();
        while(!checkNTP_TimeSync() && millis() - startAttemptTime < WIFI_TIMEOUT_MS) {
          vTaskDelay(1000 / portTICK_PERIOD_MS);    
        }

        if(!checkNTP_TimeSync()) {
          Serial.println("Failed to obtain NTP time-sync data!, retrying...");
          continue;
        }
        else {
          printLocalTime();
          ntpTimeSyncDone = true;
        }
      }

      // Configure WiFiClientSecure to use the AWS IoT device credentials
      net.setCACert(AWS_CERT_CA);
      net.setCertificate(AWS_CERT_CRT);
      net.setPrivateKey(AWS_CERT_PRIVATE);

      // Connect to the MQTT broker on the AWS endpoint we defined earlier
      client.setOptions(30, true, 1000);
      client.begin(AWS_IOT_ENDPOINT, 8883, net);

      // Create a message handler
      client.onMessage(messageHandler);

      Serial.println("Connecting to AWS IOT");

      startAttemptTime = millis();
      while (!client.connect(THINGNAME) && millis() - startAttemptTime < WIFI_TIMEOUT_MS) {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
      }

      if(!client.connected()) {
        Serial.println("AWS IoT Timeout!");
        vTaskDelay(5000 / portTICK_PERIOD_MS);    
        continue;
      }
      else 
      {
        if(!awsIoTSetupDone) {
          Serial.println("AWS IoT Connected!");      
          awsIoTSetupDone = true;
        }

        startAttemptTime = millis();
        while(!client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC, 1)) {
          Serial.print("Trying subscribe to topic (QOS_1): ");
          Serial.println(AWS_IOT_SUBSCRIBE_TOPIC);
          vTaskDelay(1000 / portTICK_PERIOD_MS);
        }

        if(client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC, 1)) {
          Serial.print("Subscribe to topic (QOS_1): ");
          Serial.println(AWS_IOT_SUBSCRIBE_TOPIC);
          subscribeToTopic = true;
        }
        else {
          Serial.print("FAILED TO Subscribe to topic (QOS_1): ");
          Serial.println(AWS_IOT_SUBSCRIBE_TOPIC);
          subscribeToTopic = false;
          continue;
        }
      }  
    }
  }
}

void setup() {
  Serial.begin(9600);

  if (!bme.begin(0x76)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }

  delay(500); // Pause for 2 seconds

  //create a task that will be executed in the keepWiFiAlive() function, with priority 1 and executed on core 1
  xTaskCreatePinnedToCore(
    keepWiFiAlive,
    "keepWiFiAlive",  // Task name
    5000,             // Stack size (bytes)
    NULL,             // Parameter
    1,                // Task priority
    NULL,             // Task handle
    1                 // pin task to core 1, main core for ESP32
  );
  delay(500);

  //create a task that will be executed in the CheckBME280Data() function, with priority 1 and executed on core 0
  xTaskCreatePinnedToCore(
                    CheckSensorData,      /* Task function. */
                    "CheckSensorData",    /* name of task. */
                    10000,                /* Stack size of task */
                    NULL,                 /* parameter of the task */
                    1,                    /* priority of the task */
                    NULL,                 /* Task handle to keep track of created task */
                    1);                   /* pin task to core 0 */    
  delay(500);
}

void loop()
{
  // Don't use this anymore - using the xTaskCreatePinnedToCore to run in task-mode
}